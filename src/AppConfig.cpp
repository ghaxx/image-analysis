//
// Created by Kuba Sławiński on 21/10/2013.
// Copyright (c) 2013 Kuba Sławiński. All rights reserved.
//


#include "AppConfig.h"

int AppConfig::argc = 0;
char** AppConfig::argv = 0;
const std::string AppConfig::outputDir = std::string("/Users/ghaxx/Development/Image Processing/Image Analysis/out");
const std::string AppConfig::inputDir = std::string("/Users/ghaxx/Development/Image Processing/Image Analysis/in");